import React, { Component } from 'react';
import { Grid, Row, Col, Button } from 'react-bootstrap';
import _ from 'lodash';
import Card from 'components/Card/Card.js';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import Axios from 'axios';
import StatsCard from 'components/StatsCard/StatsCard';

const SAMPLE_SPREADSHEET_ID = '1r1VxluO6GWJ-n589fAdafiSc2erGrAjaqZ-YbYL8OTI';
const SAMPLE_RANGE_NAME = 'Response_Testing';

class TableList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            responseData: [],
            spreadsheetId: '',
            sheetName: '',
            email: '',
            password: '',
        };
    }

    createLegend(json) {
        var legend = [];
        for (var i = 0; i < json['names'].length; i++) {
            var type = 'fa fa-circle text-' + json['types'][i];
            legend.push(<i className={type} key={i} />);
            legend.push(' ');
            legend.push(json['names'][i]);
        }
        return legend;
    }

    _handleUsernameChange = (event) => {
        this.setState({ email: event.target.value });
    };

    _handleSubmit = (event) => {
        event.preventDefault();
        const { email, password, sheetName } = this.state;
        this.setState({ sheetName: email });
    };

    _renderAPI = (sheet_name) => {
        return Axios.get(
            `http://127.0.0.1:5000/spreadsheet/${
                _.isEmpty(sheet_name) ? SAMPLE_RANGE_NAME : sheet_name
            }`
        )

            .then((response) => {
                this.setState({
                    responseData: response.data,
                });
            })
            .catch((error) => {
                console.warn(error);
            });
    };

    render() {
        const {
            responseData,
            sheetName,
            spreadsheetId,
            email,
            password,
        } = this.state;
        this._renderAPI(sheetName);
        const columnName = [];
        const customData = [];
        _.forEach(responseData, (val, key) => {
            customData.push(_.assignIn({ id: key }, val));
        });
        const head = _.head(customData);
        _.forEach(head, (val, key) => {
            columnName.push(key);
        });
        console.log('columnName', columnName);

        const toRenderDataTable = _.orderBy(customData, _.head(columnName), [
            'desc',
        ]);
        console.log('toRenderDataTable', toRenderDataTable);

        console.log('Sheet info', email);
        return (
            <div className="content">
                <Grid fluid>
                    <Row>
                        <Col lg={4} sm={4}>
                            <StatsCard
                                bigIcon={
                                    <i className="fa fa-heartbeat text-warning" />
                                }
                                statsText="Respondents"
                                statsValue={_.size(toRenderDataTable)}
                                statsIcon={<i className="fa fa-refresh" />}
                                statsIconText="Updated now"
                            />
                        </Col>
                        <Col lg={4} sm={4}>
                            <div>
                                <form
                                    className="form-signin"
                                    onSubmit={this._handleSubmit}
                                >
                                    <input
                                        type="text"
                                        id="inputEmail"
                                        name="email"
                                        className="form-control"
                                        placeholder="Your sheet name"
                                        onChange={this._handleUsernameChange}
                                    />
                                    <button
                                        className="btn btn-lg btn-info btn-block mt-4 mb-1"
                                        type="submit"
                                    >
                                        Search
                                    </button>
                                </form>
                            </div>
                        </Col>
                    </Row>

                    <Row>
                        <Col md={12}>
                            <Card
                                title="Google form respondent"
                                category="Here is a subtitle for this table"
                                ctTableFullWidth
                                ctTableResponsive
                                content={
                                    <BootstrapTable
                                        data={toRenderDataTable}
                                        insertRow
                                        deleteRow
                                        exportCSV
                                        search
                                        hover
                                        keyField="id"
                                        pagination
                                        headerStyle={{
                                            background: '#325439',
                                            font: '#fcba03',
                                            color: '#fcba03',
                                        }}
                                    >
                                        {columnName.map((column) => (
                                            <TableHeaderColumn
                                                dataAlign="center"
                                                dataField={column}
                                                hidden={
                                                    _.isEqual(column, 'id')
                                                        ? true
                                                        : false
                                                }
                                            >
                                                {column}
                                            </TableHeaderColumn>
                                        ))}
                                    </BootstrapTable>
                                }
                            />
                        </Col>
                    </Row>
                </Grid>
            </div>
        );
    }
}

export default TableList;
