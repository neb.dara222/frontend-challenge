import Dashboard from 'views/Dashboard.js';
import UserProfile from 'views/UserProfile.js';
import TableList from 'views/TableList.js';

const dashboardRoutes = [
    {
        path: '/general',
        name: 'General',
        icon: 'pe-7s-graph',
        component: Dashboard,
        layout: '/admin',
    },
    {
        path: '/data',
        name: 'Data',
        icon: 'pe-7s-note2',
        component: TableList,
        layout: '/admin',
    },
    {
        path: '/prediction',
        name: 'Prediction',
        icon: 'pe-7s-user',
        component: UserProfile,
        layout: '/admin',
    },
];

export default dashboardRoutes;
